import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://au-api.basiq.io/'
});

export default instance;
