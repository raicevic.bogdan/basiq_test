// FIXME: Move from hooks to Class component
import React, { Fragment, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import UserInputField from './UserInputField';

const ConnectToUserForm = (props) => {
  const [validated, setValidated] = useState(false);

  // FIXME: Unclick button after validation
  const handleSubmit = event => {
    event.preventDefault();

    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.stopPropagation();
    }

    let connectionToValidateAndCreate = {
      loginId: event.target[0].value,
      password: event.target[1].value,
      institution: {
        id: event.target[2].value
      }
    };

    setValidated(true);
    if (event.target.checkValidity()) {
      props.createConnection(connectionToValidateAndCreate);
    }
  };

  let userLoginId = {
    formLabel: 'Login Id',
    inputType: 'text',
    inputMinLength: '8',
    inputMaxLength: '50',
    inputPlaceholder: 'gavinBelson',
    inputPattern: '(.*?)',
    formInvalidMessage:
      'Please enter a your user Id, with mininum of 8 characters.'
  };

  let userPassword = {
    formLabel: 'Password',
    inputType: 'password',
    inputMinLength: '8',
    inputMaxLength: '50',
    inputPlaceholder: 'hooli2016',
    inputPattern: '(.*?)',
    formInvalidMessage:
      'Please enter your password, with mininum of 8 characters'
  };

  let institutionId = {
    formLabel: 'Institution Id',
    inputType: 'text',
    inputMinLength: '7',
    inputMaxLength: '7',
    inputPlaceholder: 'AU00000',
    inputPattern: '[A-Z]{2}[0-9]{5}',
    formInvalidMessage:
      'Please enter institution Id. Id must me exaclty 7 characters in size'
  };

  return (
    <Fragment>
        <Form noValidate validated={validated} onSubmit={handleSubmit}>
          <Form.Row>
            <UserInputField formData={userLoginId}></UserInputField>
          </Form.Row>
          <Form.Row>
            <UserInputField formData={userPassword}></UserInputField>
          </Form.Row>
          <Form.Row>
            <UserInputField formData={institutionId}></UserInputField>
          </Form.Row>
          <Form.Row>
            <Button type="submit">Create connection to user</Button>
          </Form.Row>
        </Form>
    </Fragment>
  );
};

export default ConnectToUserForm;
