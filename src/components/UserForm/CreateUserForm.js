// FIXME: Move from hooks to Class component
import React, { Fragment, useState } from 'react';
import { Button, Form } from 'react-bootstrap';
import UserInputField from './UserInputField';

const CreateUserForm = props => {
  const [validated, setValidated] = useState(false);

  // FIXME: Unclick button after validation
  const handleSubmit = event => {
    event.preventDefault();

    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.stopPropagation();
    }

    // FIXME: this is a hack
    let userToValidateAndCreate = {
      email: event.target[0].value
    };
    
    setValidated(true);
    if (event.target.checkValidity()) {
      props.createUser(userToValidateAndCreate);
    }
  };

  let userEmail = {
    formLabel: 'Email',
    inputType: 'email',
    inputMinLength: '3',
    inputMaxLength: '50',
    inputPlaceholder: 'you@mail.com',
    // eslint-disable-next-line
    inputPattern:
      "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$",
    formInvalidMessage:
      'Please enter a proper email address in following format you@mail.com'
  };

  return (
    <Fragment>
      <Form noValidate validated={validated} onSubmit={handleSubmit}>
        <Form.Row>
          <UserInputField formData={userEmail}></UserInputField>
        </Form.Row>
        <Form.Row>
          <Button type="submit">Create User</Button>
        </Form.Row>
      </Form>
    </Fragment>
  );
};

export default CreateUserForm;
