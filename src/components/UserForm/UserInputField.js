import React from 'react';
import {Form, Col, InputGroup} from 'react-bootstrap';

const UserInputField = (props) => {
  return (
    <Form.Group as={Col} >
      <Form.Label>{props.formData.formLabel}</Form.Label>
      <InputGroup>
        <Form.Control
          type={props.formData.inputType}
          minLength={props.formData.inputMinLength}
          maxLength={props.formData.inputMaxLength}
          placeholder={props.formData.inputPlaceholder}
          pattern={props.formData.inputPattern}
          required
        />
        <Form.Control.Feedback type="invalid">
          {props.formData.formInvalidMessage}
        </Form.Control.Feedback>
      </InputGroup>
    </Form.Group>
  );
};

export default UserInputField;
