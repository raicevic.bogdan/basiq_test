import React, { Component, Fragment } from 'react';
import style from './BasiqLayout.module.css';
import axios from '../../axios-instance';
import { Button, Container, Row, Col } from 'react-bootstrap';
import TransactionsTable from '../TransactionsTable/TransactionsTable';
import Logo from '../Logo/Logo';
import Spinner from '../Spinner/Spinner';
import CreateUserForm from '../UserForm/CreateUserForm';
import ConnectToUserForm from '../UserForm/ConnectToUserForm';
import AverageStatisticsTable from '../TransactionStatistics/AverageStatisticsTable';

class BasiqLayout extends Component {
  state = {
    user: {},
    connection: {},
    isConnectionSuccesfull: false,
    jobStatus: {},
    jobStatusMessage: '',
    transactions: {
      data: []
    },
    loading: false,
    error: false,
    errorMessage: 'No requests for transactions made. '
  };

  createUserHandler = userToCraete => {
    axios
      .post('/users', userToCraete, {
        headers: {
          Authorization: sessionStorage.getItem('access_token'),
          Accept: 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      })
      .then(response => {
        this.setState({ user: response.data });
        sessionStorage.setItem('userId', response.data.id);
      })
      .catch(error => {
        this.setState({
          error: true,
          errorMessage: this.getErrorMessage(error)
        });
      });
  };

  createConnectionWithUserIdHandler = connectionFormData => {
    axios
      .post(
        '/users/' + sessionStorage.getItem('userId') + '/connections',
        connectionFormData,
        {
          headers: {
            Authorization: sessionStorage.getItem('access_token'),
            'Content-Type': 'application/json',
            Accept: 'application/json'
          }
        }
      )
      .then(response => {
        this.setState({ connection: response.data });
        sessionStorage.setItem('jobId', response.data.id);
        this.getJobStatusHandler();
      })
      .catch(error => {
        this.setState({
          error: true,
          errorMessage: this.getErrorMessage(error)
        });
      });
  };

  getAllTransactionsForSingleBankHandler = () => {
    this.setState({ loading: true });
    axios
      .get(
        '/users/' +
          sessionStorage.getItem('userId') +
          "/transactions?filter=transaction.id.eq('AU00000')",
        {
          headers: {
            Authorization: sessionStorage.getItem('access_token'),
            Accept: 'application/json'
          }
        }
      )
      .then(response => {
        this.setState({ loading: false, transactions: response.data });
      })
      .catch(error => {
        this.setState({
          loading: false,
          error: true,
          errorMessage: this.getErrorMessage(error)
        });
      });
  };

  getJobStatusHandler = () => {
    this.setState({loading: true});
    axios
      .get('/jobs/' + sessionStorage.getItem('jobId'), {
        headers: {
          Authorization: sessionStorage.getItem('access_token'),
          'Content-Type': 'application/json',
          Accept: 'application/json'
        }
      })
      .then(response => {
        this.setState({ jobStatus: response.data });
        console.log(this.state.jobStatus);
        if (this.state.jobStatus['steps'][2]['status'] === 'failed') {
          this.setState({
            jobStatusMessage: 'Fatal error in connection to user',
            loading: false
          });
        } else if (this.state.jobStatus['steps'][2]['status'] === 'success') {
          this.setState({
            jobStatusMessage: '',
            isConnectionSuccesfull: true,
            loading: false
          });
        } else {
          this.setState({ jobStatusMessage: 'Working on it' });
          setTimeout(this.getJobStatusHandler, 4000);
        }
      })
      .catch(error => {
        this.setState({
          error: true,
          errorMessage: this.getErrorMessage(error),
          loading: false
        });
      });
  };

  getErrorMessage = error => {
    return error.response.data.data[0].detail;
  };

  render() {
    let errorMessage = this.state.errorMessage;
    if (this.state.transactions.data.length === 0)
      errorMessage += ' Sorry, no data found for user.';
    let tableAndStatistics = (
      <p>User data could not be loaded. {errorMessage}</p>
    );

    let transactionsButton = '';
    if (this.state.isConnectionSuccesfull) {
      transactionsButton = (
        <Button
          className={style.ButtonBasiq}
          onClick={this.getAllTransactionsForSingleBankHandler}
        >
          Get transactions
        </Button>
      );
    }

    if (this.state.transactions.data.length > 0 && this.state.error === false) {
      tableAndStatistics = (
        <Fragment>
          <Col lg="8">
            <TransactionsTable
              className={style.ElementSpacing}
              transactions={this.state.transactions.data}
            ></TransactionsTable>
          </Col>
          <Col lg="4">
            <AverageStatisticsTable
              transactions={this.state.transactions.data}
            ></AverageStatisticsTable>
          </Col>
        </Fragment>
      );
    }

    if (this.state.loading) {
      tableAndStatistics = <Spinner />;
    }

    return (
      <Fragment>
        <Container>
          <div className={style.ElementSpacing}>
            <Logo />
          </div>

          <Row>
            <Col lg="6">
              <div className={style.ElementSpacing}>
                <CreateUserForm
                  createUser={this.createUserHandler}
                ></CreateUserForm>
              </div>
            </Col>
            <Col lg="6">
              <div className={style.ElementSpacing}>
                <ConnectToUserForm
                  createConnection={this.createConnectionWithUserIdHandler}
                ></ConnectToUserForm>
                <p>{this.state.jobStatusMessage}</p>
              </div>
            </Col>
          </Row>
          <Row>{transactionsButton}</Row>
          <Row>{tableAndStatistics}</Row>
        </Container>
      </Fragment>
    );
  }
}

export default BasiqLayout;
