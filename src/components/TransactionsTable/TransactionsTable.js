import React from 'react';
import ReactTable from 'react-table';
import moment from 'moment';
import 'react-table/react-table.css';
import './fixTable.css';

const TransactionTable = props => {
  let curentBalance = 0;

  const columns = [
    {
      Header: 'Behavior',
      columns: [
        {
          Header: 'Balance',
          accessor: 'balance'
        },
        {
          Header: 'Amount',
          accessor: 'amount'
        },
        {
          Header: 'Direction',
          accessor: 'direction'
        },
        {
          Header: 'Post date',
          accessor: 'postDate'
        }
      ]
    },
    {
      Header: 'Class',
      columns: [
        {
          Header: 'Sub Class',
          accessor: 'subClass.code'
        },
        {
          Header: 'Title',
          accessor: 'subClass.title'
        }
      ]
    }
  ];
  const data = props.transactions.map(row => {
    curentBalance = curentBalance + parseInt(row.amount);
    row.balance = curentBalance;
    row.postDate = moment(row.postDate).format('YYYY-MM-DD');
    return row;
  });

  return <ReactTable data={data} columns={columns}></ReactTable>;
};

export default TransactionTable;
