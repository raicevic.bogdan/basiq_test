import React from 'react';
import HashMap from 'hashmap';
import Table from 'react-bootstrap/Table';

const AverageStatistics = props => {
  let transactionsHashMap = new HashMap();

  props.transactions.forEach(row => {
    let subClass = row.subClass;
    if (subClass) {
      if (transactionsHashMap.has(row.subClass.code)) {
        let newValue = {
          title: row.subClass.title,
          counter: transactionsHashMap.get(row.subClass.code).counter + 1,
          sum:
            parseInt(transactionsHashMap.get(row.subClass.code).sum) +
            parseInt(row.amount),
          avg:
            transactionsHashMap.get(row.subClass.code).sum /
            transactionsHashMap.get(row.subClass.code).counter
        };
        transactionsHashMap.remove(row.subClass.code);
        transactionsHashMap.set(row.subClass.code, newValue);
      } else {
        transactionsHashMap.set(row.subClass.code, {
          title: row.subClass.title,
          counter: 1,
          sum: row.amount,
          avg: row.amount
        });
      }
    } else {
      const uncoded = 'Uncoded';
      if (transactionsHashMap.has(uncoded)) {
        let newValue = {
          title: uncoded,
          counter: transactionsHashMap.get(uncoded).counter + 1,
          sum:
            parseInt(transactionsHashMap.get(uncoded).sum) +
            parseInt(row.amount),
          avg:
            transactionsHashMap.get(uncoded).sum /
            transactionsHashMap.get(uncoded).counter
        };
        transactionsHashMap.remove(uncoded);
        transactionsHashMap.set(uncoded, newValue);
      } else {
        transactionsHashMap.set(uncoded, {
          title: uncoded,
          counter: 1,
          sum: row.amount,
          avg: row.amount
        });
      }
    }
  });

  let averages = [];
  transactionsHashMap.forEach((value) => {
    averages.push({ title: value.title, avg: value.avg });
  });

  return (
    <Table responsive striped bordered>
      <thead>
        <tr><th colSpan="2">Average expenditures</th></tr>
        <tr>
          <th>Class</th>
          <th>Amount</th>
        </tr>
      </thead>
      <tbody>
        {averages.map(item => {
          return <TableRow key={item.title} row={item}></TableRow>;
        })}
      </tbody>
    </Table>
  );
};

const TableRow = props => {
  return (
    <tr>
      <td>{props.row.title}</td>
      <td>{Number(props.row.avg).toFixed(2)}</td>
    </tr>
  );
};
export default AverageStatistics;
