import React from 'react';
import { ReactComponent as BasiqLogo } from '../../assets/images/basiq.svg';

const Logo = () => {
  return <BasiqLogo />;
};

export default Logo;
