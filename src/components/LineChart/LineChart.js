import React from 'react';
import { ResponsiveLine } from '@nivo/line';
import moment from 'moment';

const LineChart = props => {
  let income = {
    id: 'Income',
    color: 'green',
    data: []
  };

  let expenses = {
    id: 'Expenses',
    color: 'red',
    data: []
  };
  props.transactions.map(element => {
    if (element.amount >= 0) {
      income.data.push({
        x: moment(element.postDate).format('YYYY-MM'),
        y: element.amount
      });
    } else {
      expenses.data.push({
        x: moment(element.postDate).format('YYYY-MM'),
        y: element.amount
      });
    }
  });

  income.data.sort(
    (current, next) => new moment(current.x) - new moment(next.x)
  );
  expenses.data.sort(
    (current, next) => new moment(current.x) - new moment(next.x)
  );

  return (
    <div style={{ height: '500px', width: '100%' }}>
      <ResponsiveLine
        data={[income, expenses]}
        margin={{ top: 50, right: 110, bottom: 50, left: 60 }}
        xScale={{ type: 'point' }}
        yScale={{ type: 'linear', stacked: false, min: 'auto', max: 'auto' }}
        axisTop={null}
        axisRight={null}
        axisBottom={{
          orient: 'bottom',
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 0,
          legend: 'date of transaction',
          legendOffset: 36,
          legendPosition: 'middle'
        }}
        axisLeft={{
          orient: 'left',
          tickSize: 5,
          tickPadding: 5,
          tickRotation: 0,
          legend: 'Amount',
          legendOffset: -40,
          legendPosition: 'middle'
        }}
        colors={{ scheme: 'pastel1' }}
        pointSize={10}
        pointColor={{ theme: 'background' }}
        pointBorderWidth={2}
        pointBorderColor={{ from: 'serieColor' }}
        pointLabel="y"
        pointLabelYOffset={-12}
        useMesh={true}
        legends={[
          {
            anchor: 'bottom-right',
            direction: 'column',
            justify: false,
            translateX: 100,
            translateY: 0,
            itemsSpacing: 0,
            itemDirection: 'left-to-right',
            itemWidth: 80,
            itemHeight: 20,
            itemOpacity: 0.75,
            symbolSize: 12,
            symbolShape: 'circle',
            symbolBorderColor: 'rgba(0, 0, 0, .5)',
            effects: [
              {
                on: 'hover',
                style: {
                  itemBackground: 'rgba(0, 0, 0, .03)',
                  itemOpacity: 1
                }
              }
            ]
          }
        ]}
      />
    </div>
  );
};
export default LineChart;
