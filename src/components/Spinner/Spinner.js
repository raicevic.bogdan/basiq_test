import React from 'react';
import classes from './Spinner.module.css';

const spinner = () => (
  <div className={classes.Loader}>Gathering user data.</div>
);

export default spinner;
