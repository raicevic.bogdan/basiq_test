import React from 'react';
import './App.css';
import BasiqLayout from './components/BasiqLayout/BasiqLayout';
import './assets/bootstrap.min.css';
import axiosAuth from './axios-auth'

function App() {
  axiosAuth.authenticate();
  return (
    <div className="App">
      <BasiqLayout />
    </div>
  );
}

export default App;
