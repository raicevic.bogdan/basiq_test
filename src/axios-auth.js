import axios from 'axios';

function authenticate() {
  let basiqHeader = {
    crossdomain: true,
    Authorization:
      'Basic OWZiMDY4NzktZDc1OC00OTRkLWJmMGItNDEyMzg3Yzg0ODMyOmYwYTY1YWFlLTc4MGUtNGEzZC1iZjBhLTk4MjVmM2I4ODRlZA==',
    'Content-Type': 'application/x-www-form-urlencoded',
    'basiq-version': '2.0'
  };
  return axios
    .post('https://au-api.basiq.io/token', '', {
      headers: basiqHeader
    })
    .then(response => {
      const bearerToken = response.data.access_token;
      sessionStorage.setItem('access_token', 'Bearer ' + bearerToken);
    })
    .catch(error => {
      console.log(
        "Could not acquire token. If you are seeing this you are probably a dev (Nice). It also means you are runing a browser taht has CORS in place. Here's a tip disable web security ;) "
      );
    });
}

export default { authenticate };
